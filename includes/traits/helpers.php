<?php
use GuzzleHttp\Client;
use GuzzleHttp\Promise;

trait Helpers {
    private static final function asyncRequests(array $requests): array
    {
        // Guzzle with Async and promises
        // https://medium.com/@ardanirohman/how-to-handle-async-request-concurrency-with-promise-in-guzzle-6-cac10d76220e

        // Generators - yield
        // https://www.php.net/manual/en/language.generators.syntax.php

        // Guzzle with generators
        // http://docs.guzzlephp.org/en/stable/quickstart.html#concurrent-requests

        // Placeholders for the data we are handling
        $responses = [];

        // Setup a guzzle client
        $client = new \GuzzleHttp\Client([
            'timeout' => 30,
            'verify' => false // Some Fibre clients need this. Morpheus gym design, unicef forms
        ]);

        // We need to pass in $fulfilled and $rejected by reference so we can
        // push data in to it and return everything when we are done.
        // https://github.com/guzzle/guzzle/issues/1155#issuecomment-117836887
        $pool = new \GuzzleHttp\Pool($client, self::generateRequests($requests), [
            'concurrency' => 100,
            'fulfilled' => function ($response, $index) use (&$responses) {
                $responses[$index] = json_decode($response->getBody());
            },
            'rejected' => function ($reason, $index) use (&$responses) {
                $responses[$index] = $reason;
            },
        ]);
        // Initiate the transfers and create a promise
        $promise = $pool->promise();

        // Force the pool of requests to complete.
        $response = $promise->wait();

        return $responses;
    }

    private static function generateRequests(array $requests)
    {
        foreach($requests as $key => $request){

            $method = !empty($request['type']) ? $request['type'] : 'GET';
            $url = $request['url'];
            $timeout = !empty($request['timeout']) ? $request['timeout'] : 10;
            $headers = !empty($request['headers']) ? $request['headers'] : [];

            $headers['timeout'] = $timeout;
            // https://stackoverflow.com/questions/34577278/guzzle-not-sending-psr-7-post-body-correctly#answer-34577980
            $headers['Content-Type'] = 'application/x-www-form-urlencoded';

            $body = !empty($request['data']) ? http_build_query($request['data']) : '';

            yield $key => new \GuzzleHttp\Psr7\Request($method, $url, $headers, $body);
        }
    }

    public function calculateTransferedBytes(array $items): int {
        return array_reduce($items, function($carry, $item) {
            if(property_exists($item, 'transferSize')){
                $carry = $carry + $item->transferSize;
            }

            return $carry;
        }, 0);
    }

    public function cleanerThan($co2): float {
        // This array needs to be updated periodically with new data. This was
        // originally calculated with a database query but that was too slow at
        // scale. We can look in to writing a cron job that will generate and export
        // from the database once a month, that is then loaded in this file.

        // This array was last generated with rankings/index.php on 27/01/2020
        $percentiles = [0.00126957622871866,0.004035396817140881,0.012595561048805604,0.023304715095553624,0.036438786824583,0.050362397616329,0.064014899640461,0.077739052678226,0.092126836186624,0.10757047217165,0.125027739890344,0.140696302455872,0.15929047315768,0.177734818869488,0.19581439489964,0.21422507361825607,0.232736823359142,0.246082174332492,0.264348156430992,0.28306902111392,0.30180466482882,0.320295382181204,0.33950686554985604,0.360111566931774,0.38114308483189,0.40185357017186396,0.42035354145420606,0.4393550630164101,0.458541453493762,0.47918906703882,0.499654077413412,0.521285635156174,0.5405494875603221,0.56161428648152,0.58238456980151,0.604316363860106,0.6256429617179278,0.6478269528228661,0.6691073942929641,0.68867154881184,0.7103787320465419,0.7331362414675519,0.7562483364936439,0.780892842691506,0.80396830015467,0.8269877794821401,0.85060546199698,0.874387816802448,0.899691291111552,0.92324242726303,0.9511826145960923,0.976586133398462,1.002258239346,1.02822903453074,1.0566669431626,1.08448123862022,1.1130571798008,1.1446436812039398,1.17548103245766,1.2075157831423,1.2419762271574795,1.27780212823068,1.31343697309996,1.3535322129548801,1.3963404885134,1.43538821676594,1.4786819721653202,1.52287253339568,1.5710404823845998,1.6176354301871,1.6627899659050596,1.71503331661196,1.7731704594157403,1.8271314036959998,1.8888232850004,1.9514501162933802,2.01843049142384,2.08929918752446,2.1680425684300615,2.2538809089543,2.347435716407921,2.44446281762258,2.551568006854039,2.6716183180923796,2.8030676779506,2.947526052684458,3.1029734241542397,3.2801577012624605,3.4659335564053406,3.6858566410374,3.9539822299055203,4.2833358140900835,4.686514950833381,5.167897618200399,5.7413021838327,6.52500051792535,7.628926245040858,9.114465674521588,12.30185529895519,92.584834950345];

        foreach ($percentiles as $key => $value) {
            if($co2 < $value) {
                return (100 - $key) / 100;
            }
        }

        return 0;
    }
}
